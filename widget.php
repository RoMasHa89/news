<?php
// Creating new widget "News"
class News_Widget extends WP_Widget {

// Initialization parameters of new wigdet
public function __construct() {
parent::__construct("News_Widget", __("News Widget", 'news'),
array("description" => _("Widget that shows newslist from \"News\" plugin")));
}

// Function that constructs widget on site page
function widget( $args, $instance ) {
// Getting arguments
extract( $args );

// Getting count value from widget settings
$count = $instance['count'];

$filter = array();
//var_dump($instance); die();
if ($instance['hide_featured'] == 'on') array_push($filter, 'featured');
if ($instance['hide_archive'] == 'on') array_push($filter, 'archive');
// Getting data of 'news' post type from database

$mypost = array(
'post_type' => 'news',
'posts_per_page' => $count,
'meta_query' => array(
array(
'key'     => '_news_status_value_key',
'value'   => $filter,
'compare' => 'NOT IN',
),
),
);
$loop = new WP_Query( $mypost );

// if count of posts is greater then 0, starting to print posts content on page
if ($loop->post_count) {
echo $before_widget;
// Display the widget title
echo $before_title . __('Last news', 'news') . $after_title;

// Loop of parsing 'news' posts, while it's end
while ( $loop->have_posts()) :

// Goto next post
$loop->the_post();

// Getting data from fields of post
$status = get_post_meta( get_the_ID(), '_news_status_value_key', true );
$price = get_post_meta( get_the_ID(), '_news_price_value_key', true );

// Check for widget settings which posts to show
//if ( (!$instance['hide_featured'] == 'on' && $status == 'featured') ||  (!$instance['hide_archive'] == 'on'  && $status == 'archive'))

//Display the title
$link = get_permalink(get_the_ID());
echo "<a href=$link>";
	the_title();
	echo '</a>';
echo '<br>';

//Our variables from the widget settings.
printf( 'Price: %1$d$ [%2$s]', $price, $status);

echo '<br>';



endwhile;


echo $after_widget;
}
}

//Update the widget settings
function update( $new_instance, $old_instance ) {
$values = array();

//Strip tags from title to remove HTML. Save new settings
$values['count'] = strip_tags( $new_instance['count'] );
$values['hide_featured'] = $new_instance['hide_featured'];
$values['hide_archive'] = $new_instance['hide_archive'];

return $values;
}


function form( $instance ) {

//Set up some default widget settings.
$defaults = array( 'count' => '5', 'hide_featured' => false,  'hide_archive' => false);
$instance = wp_parse_args( (array) $instance, $defaults );

//Get widget fields values
if (!empty($instance)) {
$count = $instance['count'];
$hide_featured = $instance['hide_featured'];
$hide_archive = $instance['hide_archive'];
}

// Get widget field ID and name
$tableId = $this->get_field_id('count');
$tableName = $this->get_field_name('count');
?>
<!-- html layout widget -->
<label for="<?php echo $tableId ?>"><?php echo __('Number of news to show:', 'news')?></label><br>
<input id="<?php echo $tableId?>" type="text" name="<?php echo $tableName?>" value="<?php echo $count?>"><br>

<?php echo __('Hide news:', 'news')?><br>
<?php
// Get widget field ID and name
$tableId = $this->get_field_id('hide_featured');
$tableName = $this->get_field_name('hide_featured');
?>
<input class="checkbox" type="checkbox"<?php echo checked( $hide_featured, 'on', true)?> id="<?php echo $tableId?>" name="<?php echo $tableName?> " />
<label for="<?php echo $tableId?>"><?php echo __('Featured', 'news')?></label>
<br>

<?php
// Get widget field ID and name
$tableId = $this->get_field_id('hide_archive');
$tableName = $this->get_field_name('hide_archive');
?>
<input class="checkbox" type="checkbox" <?php echo checked( $hide_archive, 'on', true)?> id="<?php echo $tableId?>" name="<?php echo $tableName?>" />
<label for="<?php echo $tableId?>"><?php echo __('Archive', 'news')?></label>
<?php

}
}


// Register out news widget
function register_news_widget() {
	register_widget( 'News_Widget' );
}
add_action( 'widgets_init', 'register_news_widget' );

?>