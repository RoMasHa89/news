��          �       �      �     �     �     �     �            	          
   $  	   /     9     B     G     U     d     p     ~     �     �     �  �   �     P     V     b     i  	   n  -   x    �     �     �     �     �     �     �  )        C      X  !   y     �     �  #   �     �     �  #     )   3  $   ]  ?   �  '   �  �   �     �     �     �     	  !   	  O   9	   -- Add New Add New Post Alex Romantsov Archive Edit Edit News Featured Hide news: Last news New Post News News Category News Post-Type News Widget News options: No News Reviews found in Trash No News found Number of news to show: Parent News Plugin, that creates a new post type named "News" and additional widget. Also has shortcode (example: [news-shortcode count=5] ) Price Search News Status View View News Widget that shows newslist from "News" plugin Project-Id-Version: news.php conversion
Report-Msgid-Bugs-To: 
POT-Creation-Date: Thu Dec 03 2015 18:33:15 GMT+0200 (EET)
PO-Revision-Date: Thu Dec 03 2015 18:50:13 GMT+0200 (EET)
Last-Translator: 
Language-Team: 
Language: Unknown
Plural-Forms: nplurals=2; plural=n!=1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: en_UA
X-Loco-Parser: loco_parse_php
X-Loco-Target-Locale: zxx
X-Generator: Loco - https://localise.biz/ -- Добавить Добавить новость Alex Romantsov Архивная Редактировать Редактировать новости Выделенная Спрятать новости: Последние новости Новый пост Новости Категория новостей Новости Виджет новостей Настройки новостей Нет новостей в корзине Не найдено новостей Количество отображаемых новостей: Родительские новости Плагин, который создает новый пост-тайп "Новости", с виджетом. Также имеется шорт-код (пример использования: [news-shortcode count=5] )
 Стоимость Поиск новостей Статус Просмотр Просмотр новостей Виджет, который показывает список новостей 