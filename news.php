<?php
/*
Plugin Name: News Post-Type
Plugin URI: --
Description: Plugin, that creates a new post type named "News" and additional widget. Also has shortcode (example: [news-shortcode count=5] )
Version: 1.0.0
Author: Alex Romantsov
Author URI: --
Text Domain: news
Domain Path: /languages
License: No Licence
*/

// Create new post type
function create_news() {
	register_post_type( 'news',
			array(
					'labels' => array(
							'name' => __('News', 'news'),
							'singular_name' => __('News', 'news'),
							'add_new' => __('Add New', 'news'),
							'add_new_item' => __('Add New Post', 'news'),
							'edit' => __('Edit', 'news'),
							'edit_item' => __('Edit News', 'news'),
							'new_item' => __('New Post', 'news'),
							'view' => __('View', 'news'),
							'view_item' => __('View News', 'news'),
							'search_items' => __('Search News', 'news'),
							'not_found' => __('No News found', 'news'),
							'not_found_in_trash' => __('No News Reviews found in Trash', 'news'),
							'parent' => __('Parent News')
					),
					'public' => true,
					'menu_position' => 15,
					'show_ui' => true,
					'_builtin' => false,
					'capability_type' => 'post',
					'hierarchical' => false,
					'rewrite' => array("slug" => "news"),
					'supports' => array('title', 'editor', 'news'),
					'menu_icon'   => 'dashicons-format-aside'
			)
	);


}
add_action( 'init', 'create_news' );


// makes new taxonomy for news post type
function create_news_tax() {
	register_taxonomy(
			'news_type',
			'news',
			array ('hierarchical' => false, 'label' => __('News Category', 'news'),
					'singular_label' => __('News Category', 'news'),
					'query_var' => true,
					'rewrite' => true,
					'hierarchical' => true
			)
	);
}
add_action( 'init', 'create_news_tax' );


// creates custom fields on post-type 'news'
function news_add_custom_box() {
	$screens = array( 'news');
	foreach ( $screens as $screen )
		add_meta_box( 'news_sectionid', __('News options:', 'news'), 'news_meta_box_callback', $screen );
}
add_action('add_meta_boxes', 'news_add_custom_box');

// html layout of custom fields for post-type 'news'
function news_meta_box_callback() {
	// using nonce for verify
	wp_nonce_field( plugin_basename(__FILE__), 'news' );


	// out fields
	$my_fields = array('_news_status_value_key' => '', '_news_price_value_key' => '');

	foreach ($my_fields as $key => $value) {
		$my_fields[$key] = get_post_meta(get_the_ID(), $key, true);
	}

	?>
	<!-- fields for data input -->
	<label for="news_status"><?php echo __('Status', 'news')?></label> -
	<select name="news_status">
		<option <?php echo selected( $my_fields['_news_status_value_key'], 'featured', true );?> value="featured"><?php echo __('Featured', 'news')?></option>

		<option <?php echo selected( $my_fields['_news_status_value_key'], 'archive', true );?> value="archive"><?php echo __('Archive', 'news')?></option>
	</select><br>

	<label for="news_price"><?php echo __('Price', 'news')?></label> -
	<input type="text" id= "news_price" name="news_price" value=" <?php echo $my_fields['_news_price_value_key']?>" size="10" />
	<?php
}

// Save data when post is getting saved
function news_save_postdata( $post_id ) {
	// check nonce of our page for calling from our "news" form
	if ( ! wp_verify_nonce( $_POST['news'], plugin_basename(__FILE__) ) )
		return $post_id;

	// check for autosave, if true - skip saving data
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	// check user capability to edit post
	if ( 'page' == $_POST['post_type'] && ! current_user_can( 'edit_page', $post_id ) ) {
		return $post_id;
	} elseif( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// check fields is set
	if ( ! isset( $_POST['news_status'] ) && ! isset( $_POST['news_price'] ))
		return;


	// Getting data from fields and updating database
	$my_data = sanitize_text_field( $_POST['news_status'] );
	update_post_meta( $post_id, '_news_status_value_key', $my_data );

	//Getting data from fields and updating database
	$my_data = sanitize_text_field( $_POST['news_price'] );
	update_post_meta( $post_id, '_news_price_value_key', $my_data );

}
add_action( 'save_post', 'news_save_postdata', 1, 2 );

// Require a widget setup
require_once(plugin_dir_path( __FILE__ ) . 'widget.php');

// Add a shortcode for news post type
function news_shortcode_func( $atts ) {
	// Getting arguments
	extract( $atts );

	// Getting data of 'news' post type from database
	$mypost = array( 'post_type' => 'news', 'posts_per_page' => $count);
	$loop = new WP_Query( $mypost );

	// if count of posts is greater then 0, starting to print posts content on page
	if ($loop->post_count) {
		echo $before_widget;
		while ( $loop->have_posts()) :

			// Goto next post
			$loop->the_post();

			// Getting data from fields of post
			$status = get_post_meta( get_the_ID(), '_news_status_value_key', true );
			$price = get_post_meta( get_the_ID(), '_news_price_value_key', true );


			// Display the post title and price
			$link = get_permalink(get_the_ID());

			// Starting buffer usage for html out
			ob_start();
			echo "<a href=$link>";
			echo $before_title . the_title() . $after_title;
			echo '</a>';
			echo '<br>';
			echo "&emsp;Price: $price$";
			echo '<br>';
			// Get data from buffer, then clean buffer
			$html = ob_get_clean();

			echo $html;
		endwhile;


		echo $after_widget;
	}

}
add_shortcode( 'news-shortcode', 'news_shortcode_func' );

// Enable jquery
function force_post_title_init()
{
	wp_enqueue_script('jquery');
}
add_action('admin_init', 'force_post_title_init');


// Check for empty title of new post. Using a JS script in news plugin 'js' directory
function force_post_title()
{

	if (is_post_type('news')) {
		echo "<script type='text/javascript'>\n";
		echo "
	  jQuery('#publish').click(function(){
			var testervar = jQuery('[id^=\"titlediv\"]')
			.find('#title');
			if (testervar.val().length < 1)
			{
				jQuery('[id^=\"titlediv\"]').css('background', '#F96');
				setTimeout(\"jQuery('#ajax-loading').css('visibility', 'hidden');\", 100);
				alert('POST TITLE is required');
				setTimeout(\"jQuery('#publish').removeClass('button-primary-disabled');\", 100);
				return false;
			}
		});
	  ";
		echo "</script>\n";
	}
}
add_action('edit_form_advanced', 'force_post_title');


// Check for empty content of new post. Using a JS script in news plugin 'js' directory
function force_post_content()
{
	if (is_post_type('news')) {
		echo "<script type='text/javascript'>\n";
		echo "
	  jQuery('#publish').click(function(){
			var testervar = jQuery('[id^=\"wp-content-editor-container\"]')
			.find('#content');
			if (testervar.val().length < 1)
			{
				jQuery('[id^=\"wp-content-editor-container\"]').css('background', '#F96');
				setTimeout(\"jQuery('#ajax-loading').css('visibility', 'hidden');\", 100);
				alert('POST CONTENT is required');
				setTimeout(\"jQuery('#publish').removeClass('button-primary-disabled');\", 100);
				return false;
			}
		});
	  ";
		echo "</script>\n";
	}
}
add_action('edit_form_advanced', 'force_post_content');

function news_load_plugin_textdomain() {
	load_plugin_textdomain( 'news', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'news_load_plugin_textdomain' );


?>